package newtasks.io;

/* Напишите приложение находящее все файлы с заданным именем в заданной директории и
или ее поддиректориях. Для каждого найденного файла должен быть выведен полный путь*/

import java.io.File;

public class SearchFile {

    private static final String SEARCH_FILE_NAME = "";
    private static final String SEARCH_DIR = "";

    public static void main(String[] args) {
        scanFilesInDir(new File(SEARCH_DIR));
    }

    private static void scanFilesInDir(File path) {
        File[] folderEntries = path.listFiles();
        if (folderEntries != null) {
            for (File entry : folderEntries) {
                if (entry.isDirectory()) {
                    scanFilesInDir(entry);
                }
                searchFile(entry);
            }
        }
    }

    private static void searchFile(File entry) {
        if (entry.getName().equals(SEARCH_FILE_NAME)) {
            System.out.println(entry.getPath());
        }
    }
}
