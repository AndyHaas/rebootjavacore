package newtasks.io;

/* Напишите консольное приложение для выполнения действий над файловой системой.
Приложение запрашивает у пользователя действия после чего запрашивает параметры
для выбранного действия. Действия должны включать копирование, перемещение и удаление файлов. */

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Scanner;

public class CRUDApp {
    public static void main(String[] args) {
        application();
    }

    private static void application() {
        startMenu();
        checkAction();
    }

    private static void startMenu() {
        System.out.println("Выберите действие: ");
        System.out.print("1 - Будем копировать файл" + System.lineSeparator()
                + "2 - Будем перемещать файл" + System.lineSeparator()
                + "3 - Будем удалять файл" + System.lineSeparator());
    }

    private static void checkAction() {
        Scanner scanner = new Scanner(System.in);
        int action = scanner.nextInt();

        switch (action) {
            case 1:
                String src;
                String dest;
                try {
                    System.out.println("Введите путь к каталогу-первоисточнику: ");
                    src = scanner.nextLine();
                    File source = new File(src);

                    System.out.print("Введите полный путь к каталогу-приёмнику: ");
                    dest = scanner.nextLine();
                    File destination = new File(dest);

                    if (source.exists() && destination.exists()) {
                        Files.copy(source.toPath(), destination.toPath());
                    }
                } catch (IOException e) {
                    System.out.println("Копируемый файл не найден");
                    e.printStackTrace();
                }
                break;

            case 2:
                System.out.println("Введите наименование файла для перемещения.");
                String fileName = scanner.nextLine();
                System.out.println("Введите полный путь, в котором находится файл перемещаемый файл: ");
                String srcFolder = scanner.nextLine();
                File sourceFolder = new File(srcFolder);
                System.out.println("Введите полный путь, в котором находится файл перемещаемый файл: ");
                String dstFolder = scanner.nextLine();
                File destinationFolder = new File(dstFolder);
                File originalFile = new File(sourceFolder, fileName);
                File featureFile = new File(destinationFolder, fileName);

                if (originalFile.exists() && !featureFile.exists()) {
                    System.out.println("Переносится файл: " + fileName + "");
                    if (originalFile.renameTo(featureFile)) {
                        System.out.println("Перенос завершился успешно.");
                    } else {
                        System.out.println("Перенос не удался.");
                    }
                }
                break;

            case 3:
                System.out.print("Ведите полный путь(с указанием удаляемого файла)): ");
                String fullPathToDelFile = scanner.nextLine();
                File deletingFile = new File(fullPathToDelFile);

                if (deletingFile.exists() && deletingFile.isFile()) {
                    System.out.println("Удаляю файл: " + deletingFile.getName());
                    deletingFile.delete();
                }

                System.out.println(!deletingFile.exists() ? "Операция удаления завершилась успешно." : "Удалить файл не удалось.");
                break;

            default:
                System.out.println("Заявленная команда мне не известна. Повторите выбор действия: ");
                application();
                break;
        }
    }
}
