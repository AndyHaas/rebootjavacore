package newtasks.io;

/* Напишите приложение для вывода всех файлов и поддиректорий для указанной директории */

import java.io.File;
import java.util.Scanner;

public class FilesAndSubDirs {
    public static void main(String[] args) {
        String path = getPath();
        getFilesAndDirs(path);
    }

    private static void getFilesAndDirs(String path) {
        File file = new File(path);
        File[] files = file.listFiles();
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                File value = files[i];
                if (value.exists()) {
                    System.out.println(value.getAbsolutePath());
                }
            }
        }
    }

    private static String getPath() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите путь до директории для вывода файлов и поддиректорий в ней: ");
        return scanner.nextLine();
    }
}
