package newtasks.regexp;

/*
Разработайте приложение которое заменяет все вхождения номеров кредитных карт на "**** **** **** ****".
*/

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreditCard {

    private static Pattern regex = Pattern.compile("\\d{4}.\\d{4}.\\d{4}.\\d{4}");

    private static boolean isValid(String crCardNum) {
        Matcher matcher = regex.matcher(crCardNum);
        return matcher.find();
    }

    public static void main(String[] args) {
        String crCardNum = "4276 8020 1234 5678";
        String crCardMask = "**** **** **** ****";
        verify(crCardNum, crCardMask);
    }

    private static void verify(String crCardNum, String crCardMask) {
        System.out.println(isValid(crCardNum) ? crCardMask : crCardNum);
    }
}
