package newtasks.regexp;

/*
Разработайте приложение которое находит все вхождения номера телефона в формате "(415) 555-1212"
и выводит положения первого и последнего символа.
*/

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumber {

    public static void main(String[] args) {
        final String regex = ".\\d{3}\\D{2}\\d{3}.\\d{4}";
        final String string = "(415) 555-1212";

        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(string);

        while (matcher.find()) {
            System.out.println("First symbol: " + matcher.group(0).indexOf("("));
            System.out.println("Last symbol: " + matcher.group(0).indexOf("2", 12));
        }
    }
}
