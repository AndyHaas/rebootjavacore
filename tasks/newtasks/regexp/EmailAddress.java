package newtasks.regexp;

/*
Разработайте приложение которое запрашивает имя пользователя и адрес электронной почты.
Если адрес электронной почты введен неправильно необходимо запросить его повтоно.
*/

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailAddress {

    private static final Pattern VALID_EMAIL_ADDRESS =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static void main(String[] args) {
        getName();
        getEmail();
    }

    private static void getEmail() {
        Scanner input = new Scanner(System.in);
        System.out.print("Введите ваш eMail: ");
        String eMail = input.nextLine();

        while (!isValid(eMail)) {
            System.out.println("Вы ввели некорректное значение, повторите ввод: ");
            eMail = input.nextLine();
        }
    }

    private static void getName() {
        Scanner name = new Scanner(System.in);
        System.out.print("Введите ваше имя: ");
        name.nextLine();
    }

    private static boolean isValid(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS.matcher(emailStr);
        return matcher.find();
    }
}
