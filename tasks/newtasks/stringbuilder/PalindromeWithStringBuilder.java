package newtasks.stringbuilder;

/*
Разработайте метод который позволяет определить что строка является палиндромом.
Используйте метод reverse из класса StringBuilder.
*/

public class PalindromeWithStringBuilder {
    public static void main(String[] args) {
        String string = "qwertyuiooiuytrewq";
        System.out.println(isPalindrome(string));
        System.out.println(isPalindrome(null));
    }

    private static boolean isPalindrome(String string) {
        if (string == null)
            return false;
        StringBuilder sb = new StringBuilder(string);
        sb.reverse();
        return sb.toString().equals(string);
    }
}
