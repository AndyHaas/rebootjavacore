package newtasks.stringbuilder;

/*
Разработайте метод для удаления дубликатов из строки. Используйте StringBuilder и метод indexOf.
*/

public class IndexOf {
    public static void main(String[] args) {
        String s = "weqdwdfwqs";
        System.out.println(removeDuplicates(s));
    }

    private static String removeDuplicates(String string) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < string.length(); i++) {
            String substr = string.substring(i, i + 1);

            if (builder.indexOf(substr) == -1) {
                builder.append(substr);
            }
        }

        return builder.toString();
    }
}
