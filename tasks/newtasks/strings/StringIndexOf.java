package newtasks.strings;

/*Разработайте метод который позволяет подсчитать число вхождения другой строки с помощью метода indexOf.*/

public class StringIndexOf {
    public static void main(String[] args) {
        String string = "oiuytqwertyasdfgzxcvbqcdcsy";
        System.out.printf("Указанная подстрока начинается с %s индекса первоисточника.\n", string.indexOf("qwerty"));
    }
}
