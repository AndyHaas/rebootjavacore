package newtasks.strings;

/*
Разработайте метод который позволяет определить что строка является палиндромом.
Используйте подход meet-in-the-middle.
*/

public class Palindrome {
    public static void main(String[] args) {
        Palindrome palindrome = new Palindrome();
        System.out.println(palindrome.isPalindrome("asdfggfdsa"));
        System.out.println(palindrome.isPalindrome("adfsfbvfbvfbv"));
    }

    private boolean isPalindrome(String string) {
        int leftSide = 0;
        int rightSide = string.length() - 1;

        while (rightSide > leftSide) {
            if (string.charAt(leftSide) != string.charAt(rightSide)) {
                return false;
            }

            leftSide++;
            rightSide--;
        }

        return true;
    }
}
