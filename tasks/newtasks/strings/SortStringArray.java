package newtasks.strings;

/*
Разработайте метод который позволяет провести сортировку массива строк
по длине.
*/

import java.util.Arrays;

public class SortStringArray {
    public static void main(String[] args) {
        String[] strArr = initArray();
        printArray(strArr);
        printSortedArray(strArr);
    }

    private static void printSortedArray(String[] strArr) {
        String tmp;
        System.out.println("\n\nОтсортировано по возрастанию длины строки: ");

        for (int i = strArr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (strArr[j].length() > strArr[j + 1].length()) {
                    tmp = strArr[j];
                    strArr[j] = strArr[j + 1];
                    strArr[j + 1] = tmp;
                }
            }
        }

        System.out.println(Arrays.toString(strArr));
    }

    private static void printArray(String[] strArr) {
        System.out.println("Получен следующий массив строк: ");

        for (String s : strArr) {
            System.out.print(s + " ");
        }
    }

    private static String[] initArray() {
        String[] strArr = new String[10];
        strArr[0] = "Aedsvxvzcad";
        strArr[1] = "Ab";
        strArr[2] = "Abc";
        strArr[3] = "Abde";
        strArr[4] = "Abfqds";
        strArr[5] = "AbsdccA";
        strArr[6] = "Abcavs";
        strArr[7] = "Abcavsvsa";
        strArr[8] = "Abcsa";
        strArr[9] = "Abcscv";
        return strArr;
    }
}
