package newtasks.strings;

/*
Разработайте метод который позволяет найти наибольший общий префикс для
строк из массива.
*/

public class Prefix {
    public static void main(String[] args) {
        Prefix prefix = new Prefix();
        System.out.println(prefix.largestCommonPrefix());
    }

    private String largestCommonPrefix() {
        String[] prefixArray = initArray();
        int length = 0;
        String string = null;

        for (String s : prefixArray) {
            if (s.length() > length) {
                length = s.length();
                string = s;
            }
        }
        return string;
    }

    private String[] initArray() {
        String[] prefixArray = new String[10];
        prefixArray[0] = "A";
        prefixArray[1] = "Ab";
        prefixArray[2] = "Abc";
        prefixArray[3] = "Abd";
        prefixArray[4] = "Abf";
        prefixArray[5] = "AbcA";
        prefixArray[6] = "Abc";
        prefixArray[7] = "Abc";
        prefixArray[8] = "Abc";
        prefixArray[9] = "Abc";
        return prefixArray;
    }
}
