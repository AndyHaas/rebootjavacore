package lesson1.introduction;

/*Write a program that prints a face similar to (but different from) the following:
  /////
 +"""""+
(| o o |)
 |  ^  |
 | ‘-’ |
 +-----+
*/

class P1_07{
    public static void main(String[] args) {
        printHair();
        printForeheadAndChin();
        printEyesAndEars();
        printNose();
        printMouth();
        printForeheadAndChin();
    }

    private static void printMouth() {
        System.out.println(" | `-` | ");
    }

    private static void printNose() {
        System.out.println(" |  |  | ");
    }

    private static void printEyesAndEars() {
        System.out.println(")| O O |(");
    }

    private static void printForeheadAndChin() {
        System.out.println(" +-----+ ");
    }

    private static void printHair() {
        System.out.println(" ~|~|~|~ ");
    }
}
