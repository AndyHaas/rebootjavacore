package lesson1.introduction;

/*
Write a program that prints the balance of an account after the first, second, and
third year. The account has an initial balance of $1,000 and earns 5 percent interest
per year.
*/

class P1_04 {

    private static final int YEARS_OF_CONTRIBUTION = 3;

    public static void main(String[] args) {

        double profit, balance = 1_000.0, interestRatePercent = 5;

        for (int i = 1; i <= YEARS_OF_CONTRIBUTION; i++) {
            profit = balance * interestRatePercent / 100;
            balance += profit;
            System.out.println(String.format("%.2f RUR", balance));
        }
    }
}
