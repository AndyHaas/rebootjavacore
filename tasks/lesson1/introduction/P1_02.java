package lesson1.introduction;

/*
Write a program that prints the sum of the first ten positive integers,
1 + 2 + ... + 10.
*/

class P1_02 {

    private static final int MAX_POSITIVE_INTEGERS = 10;

    public static void main(String[] args) {
        int sum = 0;
        for (int i = 1; i <= MAX_POSITIVE_INTEGERS; i++) {
            sum += i;
        }
        System.out.println(sum);
    }
}
