package lesson1.introduction;

/*
Write a program that prints your name in large letters, such as
*    *    **    ****    ****    *   *
*    *   *  *   *   *   *   *   *   *
******  *    *  ****    ****     * *
*    *  ******  *   *   *   *     *
*    *  *    *  *    *  *    *    *
*/

class P1_06{
    public static void main(String[] args) {
        writeAndyWithLargeLetters();
    }

    private static void writeAndyWithLargeLetters() {
        System.out.println("   **   *   * ****  *   *");
        System.out.println("  *  *  **  * *   *  * * ");
        System.out.println(" *    * * * * *   *   *  ");
        System.out.println(" ****** *  ** *   *   *  ");
        System.out.println(" *    * *   * ****    *  ");
    }
}
