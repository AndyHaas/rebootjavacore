package lesson1.introduction;

/*
Write a program that prints the product of the first ten positive integers,
1 × 2 × ... × 10. (Use * to indicate multiplication in Java.)
*/

class P1_03 {

    private static final int MAX_POSITIVE_INTEGERS = 10;

    public static void main(String[] args) {
        int mult = 1;
        for (int i = 1; i <= MAX_POSITIVE_INTEGERS; i++) {
            mult *= i;
        }
        System.out.println(mult);
    }
}