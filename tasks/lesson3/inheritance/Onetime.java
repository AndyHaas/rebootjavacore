package lesson3.inheritance;

import java.text.ParseException;

public class Onetime extends Appointment {
    public Onetime(String description, int year, int month, int day) throws ParseException {
        super(description, year, month, day);
    }
    //..
}
