package lesson3.inheritance;

import java.text.ParseException;

public class Monthly extends Appointment {
    public Monthly(String description, int year, int month, int day) throws ParseException {
        super(description, year, month, day);
    }
    //..
}
