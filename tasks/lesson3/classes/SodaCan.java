package lesson3.classes;

/*Implement a class SodaCan with methods getSurfaceArea() and get­­Volume().
In the constructor, supply the height and radius of the can.

Реализуйте класс SodaCan с помощью методов getSurfaceArea () - площадь поверхности
и getVolume () - объём. В конструкторе укажите высоту и радиус банки
*/

class SodaCan {

    private float height;
    private float radius;

    private SodaCan(float radius, float height) {
        this.radius = radius;
        this.height = height;
    }

    private double getSurfaceArea() {
        return 2 * Math.PI * radius * (height + radius);
    }

    private double getVolume() {
        return Math.PI * Math.pow(radius, 2) * height;
    }

    public static void main(String[] args) {
        SodaCan can = new SodaCan(7, 5);
        System.out.printf("Площадь поверхности банки: %.2f \t\n", can.getSurfaceArea());
        System.out.printf("Объём банки: %.2f \t\n", can.getVolume());
    }
}