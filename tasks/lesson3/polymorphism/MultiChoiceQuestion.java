package lesson3.polymorphism;

import java.util.ArrayList;
import java.util.Scanner;

/*Add a class MultiChoiceQuestion to the question hierarchy of Section 9.1 that allows
 multiple correct choices. The respondent should provide all correct choices, sepa­
 rated by spaces. Provide instructions in the question text.*/

public class MultiChoiceQuestion extends ChoiceQuestion {
    private ArrayList<String> allAnswers;

    public MultiChoiceQuestion() {
        //..
    }

    public void setAnswer(String correctResponse) {
        //..
    }

//    public boolean checkAnswer(String response) {
//        //..
//    }
    
    public void display() {
        //..
    }
}
