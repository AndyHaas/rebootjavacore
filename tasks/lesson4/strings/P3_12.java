package lesson4.strings;

/*Write a program that translates a letter grade into a number grade. Letter grades are
A, B, C, D, and F, possibly followed by + or –. Their numeric values are 4, 3, 2, 1, and
0. There is no F+ or F–. A + increases the numeric value by 0.3, a – decreases it by 0.3.
However, an A+ has value 4.0.
Enter a letter grade: B-
The numeric value is 2.7.

Напишите программу, которая переводит буквенную оценку в числовую оценку. Буквенные оценки -
это A, B, C, D и F, за которыми возможно + или -. Их числовые значения: 4, 3, 2, 1 и 0. Нет F + или F–.
A+ увеличивает числовое значение на 0,3,
A- уменьшает его на 0,3.
Тем не менее, A+ имеет значение 4,0.
Введите буквенную оценку: B- Числовое значение составляет 2,7.
*/

import java.util.Scanner;

class P3_12 {
    public static void main(String[] args) {
        String input = getInputValue();
        int length = input.length();
        enumerationAssessment(input, length);
    }

    private static String getInputValue() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите буквенную оценку: ");
        return scanner.nextLine().toUpperCase();
    }

    private static void enumerationAssessment(String input, int length) {
        if (length == 1) {
            switch (input) {
                case "A":
                    System.out.println(4.0);
                    break;
                case "B":
                    System.out.println(3.0);
                    break;
                case "C":
                    System.out.println(2.0);
                    break;
                case "D":
                    System.out.println(1.0);
                    break;
                case "F":
                    System.out.println(0.0);
                    break;
                default:
                    System.out.println("Введено значение, не подпадающее под оценку");
                    break;
            }
        } else {
            switch (input) {
                case "A+":
                    System.out.println(4.0);
                    break;
                case "B+":
                    System.out.println(3.3);
                    break;
                case "C+":
                    System.out.println(2.3);
                    break;
                case "D+":
                    System.out.println(1.3);
                    break;
                case "A-":
                    System.out.println(3.7);
                    break;
                case "B-":
                    System.out.println(2.7);
                    break;
                case "C-":
                    System.out.println(1.7);
                    break;
                case "D-":
                    System.out.println(0.7);
                    break;
                case "F+":
                case "F-":
                    System.out.println("Не существующая оценка");
                    break;
                default:
                    System.out.println("Введено значение, не подпадающее под оценку");
                    break;
            }
        }
    }
}