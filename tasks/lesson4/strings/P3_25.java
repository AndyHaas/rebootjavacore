package lesson4.strings;

/*Write a program that prompts the user to provide a single character from the alpha­
bet. Print Vowel or Consonant, depending on the user input. If the user input is
not a letter (between a and z or A and Z), or is a string of length > 1, print an error
message.

Напишите программу, которая предлагает пользователю ввести один символ из алфавита. Печать гласного или согласного,
в зависимости от ввода пользователя. Если пользовательский ввод не является буквой (между a и z или A и Z)
или является строкой длиной> 1, выведите сообщение об ошибке.
*/

import java.util.Scanner;
import java.util.regex.Pattern;

class P3_25 {

    public static void main(String[] args) {
        String input = getString();
        verify(input);
    }

    private static boolean isAlphabet(String input) {
//        return Pattern.matches("[A-Za-z]", input);
        return input.charAt(0) >= 65 && input.charAt(0) <= 90 || input.charAt(0) >= 97 && input.charAt(0) <= 122;
    }

    private static boolean isVowel(String input) {
        return Pattern.matches("^(?i:[aeiouy]).*", input);
    }

    private static void verify(String input) {
        if (input.length() > 1) {
            throw new InputError("Введено больше одного символа!!!");
        } else if (!isAlphabet(input)) {
            throw new InputError("Введена не латинская буква!!!");
        }
        System.out.println(isVowel(input) ? "Введена латинская гласная буква" : "Введена латинская согласная буква");
    }

    private static String getString() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите букву латинского алфавита: ");
        return scanner.nextLine();
    }
}

class InputError extends Error {
    InputError(String message) {
        super(message);
    }
}
