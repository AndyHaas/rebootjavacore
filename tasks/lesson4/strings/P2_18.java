package lesson4.strings;

/*
Writing large letters. A large letter H can be produced like this:
 *   *
 *   *
 *****
 *   *
 *   *
It can be declared as a string literal like this:
final string LETTER_H = "*   *\n*   *\n*****\n*   *\n*   *\n";
(The \n escape sequence denotes a “newline” character that causes subsequent
characters to be printed on a new line.) Do the same for the letters E , L , and O . Then
write the message
H
E
L
L
O
in large letters.
*/

class P2_18 {

    private final static String LETTER_H = "*   *\n*   *\n*****\n*   *\n*   *\n";
    private final static String LETTER_E = "\n*****\n*\n*****\n*\n*****\n";
    private final static String LETTER_L = "\n*\n*\n*\n*\n*****\n";
    private final static String LETTER_O = "\n*****\n*   *\n*   *\n*   *\n*****";

    public static void main(String[] args) {

        P2_18 printer = new P2_18();
        printer.print(LETTER_H);
        printer.print(LETTER_E);
        printer.print(LETTER_L);
        printer.print(LETTER_L);
        printer.print(LETTER_O);
    }

    private void print (String letter) {
        System.out.print(letter);
    }
}