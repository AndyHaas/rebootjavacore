package lesson4.strings;

/*
Write a program that transforms numbers 1 , 2 , 3 , ..., 12
into the corresponding month names January , February ,
March , ..., December . Hint: Make a very long string "January
February March ..." , in which you add spaces such that each
month name has the same length. Then use sub­string to
extract the month you want.

Напишите программу, которая преобразует числа 1, 2, 3, ..., 12
в соответствующие названия месяцев январь, февраль,
Март, декабрь. Подсказка: сделайте очень длинную строку «Январь»
Февраль март ... », в которую вы добавляете пробелы так, чтобы каждое
название месяца имело такую же длину. Затем используйте подстроку для
извлечь месяца, который вы хотите.
*/

import java.util.Scanner;

class P2_19 {

    private static final String[] MONTH = {
            "January", "February", "March", "April", "May", "June", "July",
            "August", "September", "October", "November", "December"
    };

    public static void main(String[] args) {
        int input = getInput() - 1;
        viewMonth(MONTH[input]);
    }

    private static void viewMonth(String s) {
        System.out.println(s);
    }

    private static int getInput() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите код месяца от 1 до 12: ");
        return scanner.nextInt();
    }
}