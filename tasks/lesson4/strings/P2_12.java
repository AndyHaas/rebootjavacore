package lesson4.strings;

/*
File names and extensions. Write a program that prompts the user for
the drive letter ( C ), the path ( \Windows\System ), the file name ( Readme ),
and the extension ( txt ). Then print the complete file name C:\Windows\System\Readme.txt.
(If you use UNIX or a Macintosh, skip the drive name
and use / instead of \ to separate directories.)

Имена файлов и расширения. Напишите программу, которая предлагает пользователю
букву диска (C), путь (\ Windows \ System), имя файла (Readme) и расширение (txt).
Затем напечатайте полное имя файла C: \ Windows \ System \ Readme.txt.
(Если вы используете UNIX или Macintosh, пропустите имя диска и используйте / вместо \ для разделения каталогов.)
*/

class P2_12 {

    private String printFullPath() {
        String drive = "C:";
        String path = "\\Windows\\System\\";
        String fileName = "Readme";
        String fileExtension = "txt";
        String point = ".";
        return drive + path + fileName + point + fileExtension;
    }

    public static void main(String[] args) {
        P2_12 fullPath = new P2_12();
        System.out.println(fullPath.printFullPath());
    }
}