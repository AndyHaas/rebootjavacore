package lesson4.strings;/*
Printing a grid. Write a program that prints the following grid to play tic-tac-toe.
+--+--+--+
|  |  |  |
+--+--+--+
|  |  |  |
+--+--+--+
|  |  |  |
+--+--+--+
Of course, you could simply write seven statements of the form
System.out.println("+--+--+--+");
You should do it the smart way, though. Declare string variables to hold two kinds
of patterns: a comb-shaped pattern and the bottom line. Print the comb three times
and the bottom line once.
*/

import java.util.stream.IntStream;

class P2_15 {

    private void makeGrid() {
        int rowCount = 3;
        String firstLine = "+--+--+--+";
        String secondLine = "|  |  |  |";
        IntStream.rangeClosed(0, rowCount + rowCount)
                .mapToObj(i -> i % 2 == 0 ? firstLine : secondLine)
                .forEach(System.out::println);
    }

    public static void main(String[] args) {
        P2_15 grid = new P2_15();
        grid.makeGrid();
    }
}
