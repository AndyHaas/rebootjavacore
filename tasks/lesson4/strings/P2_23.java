package lesson4.strings;

/*
The following pseudo code describes how to turn a string containing a ten-digit
phone number (such as "4155551212" ) into a more readable string with parentheses
and dashes, like this: "(415) 555-1212" .
Take the substring consisting of the first three characters and surround it with "(" and ") ". This is the
area code.
Concatenate the area code, the substring consisting of the next three characters, a hyphen, and the
substring consisting of the last four characters. This is the formatted number.
Translate this pseudo code into a Java program that reads a telephone number into a
string variable, computes the formatted number, and prints it.

Следующий псевдокод описывает, как превратить строку, содержащую десятизначный номер телефона (например, «4155551212»),
в более читаемую строку с круглыми скобками и тире, например: «(415) 555-1212». Возьмите подстроку,
состоящую из первых трех символов, и окружите ее "(" и ")". Это код города. Объедините код области, подстроку,
состоящую из следующих трех символов, дефис и подстроку, состоящую из последних четырех символов.
Это отформатированный номер. Переведите этот псевдокод в программу Java, которая считывает номер телефона в строковую
переменную, вычисляет отформатированный номер и печатает его.
*/

import java.util.Scanner;

class P2_23 {
    public static void main(String[] args) {
        String input = getInputValue();
        String prefix = getPrefix(input);
        String firstGroup = getFirstGroup(input);
        String secondGroup = getSecondGroup(input);

        printFormatNumber(prefix, firstGroup, secondGroup);
    }

    private static String getInputValue() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите 10(десять) чисел: ");
        return scanner.nextLine();
    }

    private static void printFormatNumber(String prefix, String firstGroup, String secondGroup) {
        System.out.println("Отформатированный номер:" + prefix + firstGroup + secondGroup);
    }

    private static String getSecondGroup(String input) {
        return input.substring(6, 10);
    }

    private static String getFirstGroup(String input) {
        return input.substring(3, 6) + "-";
    }

    private static String getPrefix(String input) {
        return "(" + input.substring(0, 3) + ") ";
    }
}