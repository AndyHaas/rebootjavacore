package lesson2.primitives;

/*
Write a program that displays the dimensions of a letter-size
(width × height inches) sheet of paper in millimeters. There are 25.4 millimeters
per inch. Use constants and comments in your program.

Напишите программу, которая отображает размеры букв
(ширина × высота в дюймах) лист бумаги в миллиметрах.
Есть 25,4 миллиметра на дюйм. Используйте константы и комментарии в вашей программе.
*/

class P2_01 {
    public static final float MILLIMETER_IN_INCH = 25.4f;

    public static void main(String[] args) {
        float inchWidth = 8.27f;
        float inchHeight = 11.70f;

        int millisWidth = (int) (inchWidth * MILLIMETER_IN_INCH);
        int millisHeight = (int) (inchHeight * MILLIMETER_IN_INCH);
        System.out.print("Letter size in millis is: " + millisWidth + " x " + millisHeight);
    }
}