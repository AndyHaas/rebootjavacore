package lesson2.primitives;

/*
Write a program that asks the user for the lengths
of the sides of a rectangle. Then print
•    The area and perimeter of the rectangle
•    The length of the diagonal (use the Pythagorean theorem)

Напишите программу, которая запрашивает у пользователя длину
из сторон прямоугольника. Затем распечатать
• Площадь и периметр прямоугольника
• длина диагонали (используйте теорему Пифагора)
*/

import java.util.Scanner;

class P2_08 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите первое целое число: ");
        int length = scanner.nextInt();
        System.out.println("Введите второе целое число: ");
        int width = scanner.nextInt();

        System.out.printf("The area: %.2f %n", (double) length * width / 2);
        System.out.printf("The perimeter: %.2f %n", (double) (length + width) * 2);
        System.out.printf("The hypotenuse: %.2f %n", Math.sqrt(Math.pow(length, 2) + Math.pow(width, 2)));
    }
}