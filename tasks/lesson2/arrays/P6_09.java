package lesson2.arrays;

/*Write a method
 public static boolean equals(int[] a, int[] b)
 that checks whether two arrays have the same elements in the same order.*/

import java.util.Arrays;

class P6_09 {
    public static void main(String[] args) {
        int[] arrayOne = P6_06.initArray(10, 50);
        int[] arrayTwo = P6_06.initArray(10, 50);
        int[] arr1 = {1, 2, 3, 4, 5};
        int[] arr2 = {1, 2, 3, 4, 5};
        System.out.println(equals(arrayOne, arrayTwo));
        System.out.println(equals(arr1, arr2));
    }

    public static boolean equals(int[] a, int[] b) {
        return Arrays.equals(a, b);
    }
}