package lesson2.arrays;

/*Write a program that initializes an array with ten random integers and then prints
four lines of output, containing
•    Every element at an even index.
•    Every even element.
•    All elements in reverse order.
•    Only the first and last element.
*/

import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

public class P6_01 {
    public static void main(String[] args) {
        int[] array = new int[10];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10);
        }

        System.out.println("Original array:\t\t\t\t" + Arrays.toString(array));
        evenIndexElements(array);
        everyEvenElement(array);
        allElementsInReverseOrder(array);
        firstAndLastElement(array);
    }

    private static void evenIndexElements(int[] array) {
        System.out.print("EvenIndexElements:\t\t\t");
        IntStream.range(0, array.length)
                .filter(i -> i % 2 == 0)
                .mapToObj(i -> array[i] + " ")
                .forEach(System.out::print);
        System.out.println();
    }

    private static void everyEvenElement(int[] array) {
        System.out.print("EveryEvenElement:\t\t\t");
        IntStream.range(0, array.length)
                .filter(i -> array[i] % 2 == 0)
                .mapToObj(i -> array[i] + " ")
                .forEach(System.out::print);
        System.out.println();
    }

    private static void allElementsInReverseOrder(int[] array) {
        System.out.print("AllElementsInReverseOrder:\t");
        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    private static void firstAndLastElement(int[] array) {
        System.out.println("First element:\t\t\t\t" + array[0]);
        System.out.println("Last element:\t\t\t\t" + array[array.length - 1]);
    }
}
