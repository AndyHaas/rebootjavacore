package lesson2.control;

/*Write a program that reads a word and prints all substrings, sorted by length. For
example, if the user provides the input "rum" , the program prints

Напишите программу, которая читает слово и печатает все подстроки, отсортированные по длине.
Например, если пользователь вводит «rum», программа печатает
r
u
m
ru
um
rum
 */

import java.util.Scanner;

class P4_12 {
    public static void main(String[] args) {
        String input = getString();
        printAllSubstrings(input);
    }

    /**
     * Метод распечатывает подстроки полученной на входе строки
     *
     * @param input полученная строка
     */
    private static void printAllSubstrings(String input) {
        for (int i = 0; i < input.length(); i++) {
            for (int j = 0; j < input.length() - i; j++) {
                System.out.println(input.substring(j, j + i + 1));
            }
        }
    }

    /**
     * Метод считывает строку введённую с консоли
     *
     * @return считанное значение
     * @see Scanner
     */
    private static String getString() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a string: ");
        return scanner.next();
    }
}