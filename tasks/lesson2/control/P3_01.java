package lesson2.control;

/*Write a program that reads an integer and prints
whether it is negative, zero, or positive.*/

import java.util.Scanner;

class P3_01 {
    public static void main(String[] args) {
        int inputValue = getInputValue();
        compare(inputValue);
    }

    private static void compare(int inputValue) {
        if (inputValue < 0) {
            System.out.println("Введено ОТРИЦАТЕЛЬНОЕ число");
        }

        if (inputValue > 0) {
            System.out.println("Введено ПОЛОЖИТЕЛЬНОЕ число");
        }

        if (inputValue == 0) {
            System.out.println("Введено НУЛЕВОЕ значение");
        }
    }

    private static int getInputValue() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число, попробую угадать, какое оно: ");
        return scanner.nextInt();
    }
}
